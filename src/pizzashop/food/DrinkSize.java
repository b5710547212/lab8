package pizzashop.food;

public enum DrinkSize {
	NONE("None", 0),
	SMALL("Small", 20),
	MEDIUM("Medium", 30),
	LARGE("Large", 40);
	
	public final String size;
	public final double price;
	
	DrinkSize( String size , double price )
	{
		this.size = size;
		this.price = price;
	}
}

package pizzashop.food;

public enum PizzaSize {

	SMALL("Small", 120),
	MEDIUM("Medium", 160),
	LARGE("Large", 200),
	NONE("None", 0);
	
	public final String size;
	public final double price;
	
	PizzaSize( String size , double price)
	{
		this.size=size;
		this.price=price;
	}
	
}

package pizzashop.food;

/** 
 * A drink has a size and flavor.
 */
public class Drink implements OrderedItem{
	// constants related to drink size
	
	public DrinkSize size;
	public Flavor flavor;
	/**
	 * create a new drink.
	 * @param size is the size. 1=small, 2=medium, 3=large
	 */
	public Drink( int size ) {
		if(size==1)this.size=DrinkSize.SMALL;
		else if(size==2)this.size=DrinkSize.MEDIUM;
		else if(size==3)this.size=DrinkSize.LARGE;
		else this.size=DrinkSize.NONE;
	}
	
	/* 
	 * 
	 */
	public String toString() {
		return this.size + " " + (flavor==null? "": flavor.toString()) + " drink"; 
	}

	/** return the price of a drink
	 * @see pizzashop.FoodItem#getPrice()
	 */
	public double getPrice() {
		if ( this.size.price >= 0 ) return this.size.price;
		return 0.0;
	}
	
	public void flavour(Flavor flavor)
	{
		this.flavor = flavor;
	}
	
	public Object clone() {
		Drink clone;
		
		try {
			clone = (Drink) super.clone();
		} catch (CloneNotSupportedException e) {
			System.err.println("Drink.clone: " + e);
			clone = new Drink(0);
		}
		//superclass already cloned the size and flavor
		return clone;
	}
}

package pizzashop.food;

public interface OrderedItem {

	public double getPrice();
	
}
